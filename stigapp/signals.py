import config
import datetime
import pytz


from discord import SyncWebhook
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from supabase import create_client

# from .airtable_integration import AirTableService
from .models import GardenMessage, TopMessage, RewardReactionLog
from discordlogin.models import User
from stigapp.airtable_integration_supabase import AirTableServiceSupabase

SUPA = create_client(config.SUPABASE_URL, config.SUPABASE_KEY)


# @receiver(post_save, sender=TopMessage)
# def signal_update_airtable(sender, instance, created, **kwargs):
#     air_class = AirTableService()
#     air_class.update_leaderboard()


def create_or_update_garden_message(instance):
    """ Copies the current information about a message from the TopMessage table to the GardenMessage table """

    # Without sites framework, we use this
    # test = GardenMessage.objects.filter(
    #     message_id=instance.message_id
    # )
    test = GardenMessage.on_site.filter(
        message_id=instance.message_id
    )

    twitter_handle = ''
    if getattr(instance, 'twitter_handle', False) is False:
        user = User.objects.filter(discord_tag=instance.author_discord_username)
        twitter_handle = user[0].twitter_handle
    else:
        twitter_handle = instance.twitter_handle

    print("INSTANCE=========================")
    print(instance)
    print("=================================")

    if len(test) > 0:
        gm = GardenMessage.on_site.filter(message_id=instance.message_id).update(
            reactions=instance.reactions,
            reaction_count=instance.reaction_count,
            emoji_list=instance.emoji_list,
            user_count=instance.user_count,
            user_list=instance.user_list,
            unique_reaction_count=instance.unique_reaction_count,
            unique_emoji_list=instance.unique_emoji_list,
            unique_user_count=instance.unique_user_count,
            unique_user_list=instance.unique_user_list,
            counts_by_emoji=instance.counts_by_emoji,
            twitter_handle=twitter_handle
        )
        print(f"GardenMessage updated: {gm}")
    else:
        gm, created = GardenMessage.on_site.get_or_create(
            user_id=instance.author['id'],
            author=instance.author,
            author_discord_username=instance.author_discord_username,
            message_id=instance.message_id,
            created_at=instance.created_at,
            original_message=instance.original_message,
            editable_message=instance.editable_message,
            edited_at=instance.edited_at,
            reactions=instance.reactions,
            reaction_count=instance.reaction_count,
            emoji_list=instance.emoji_list,
            user_count=instance.user_count,
            user_list=instance.user_list,
            unique_reaction_count=instance.unique_reaction_count,
            unique_emoji_list=instance.unique_emoji_list,
            unique_user_count=instance.unique_user_count,
            unique_user_list=instance.unique_user_list,
            counts_by_emoji=instance.counts_by_emoji,
            jump_url=instance.jump_url,
            forum_tags=instance.forum_tags,
            message_type=instance.message_type,
            twitter_handle=twitter_handle,
            authorship_datetime=instance.authorship_datetime,
            channel_name=instance.channel_name,
            guild_name=instance.guild_name,
            extracted_urls_id=getattr(instance, 'extracted_urls_id', None),
            message_thread_title=instance.message_thread_title,
            django_site_id=instance.django_site_id
        )
        print(f"GardenMessage created: {gm}")

    garden_message = SUPA.table('garden_message').select('*').eq("message_id", instance.message_id).execute()
    inst = AirTableServiceSupabase()
    inst.update_or_create_a_single_record(config.AIRTABLE_TABLE_NAME_GARDEN, garden_message.data[0])


def trigger_reward_emoji(message_id, message_type):
    """ Sends a message_id to the admin channel to indicate the Discord bot should add a flower emoji to the message """

    # webhook = SyncWebhook.from_url(
    #     config.DISCORD_WEBHOOK)
    # webhook.send(f"{message_type} (id: {message_id}) was sent to the Greenhouse!")

    results = RewardReactionLog.objects.filter(message_id=message_id)

    tz = pytz.timezone(config.TIME_ZONE)
    time_info = tz.localize(datetime.datetime.now())

    if len(results) > 0:
        print("Already exists in database")
        pass
    else:
        RewardReactionLog.objects.create(
            created_at=time_info,
            message_id=message_id,
            message_type=message_type,
            reaction_sent=False
        )


@ receiver(post_save, sender=TopMessage)
def top_message_signal(sender, instance, created, **kwargs):
    """ Triggers when a TopMessage row is saved.

    If added_to_garden field is true, adds the message to the GardenMessage table and triggers a reward emoji for the message
    """

    if instance.added_to_garden is True:
        create_or_update_garden_message(instance)

        trigger_reward_emoji(instance.message_id, instance.message_type)
