from django.contrib import admin
from django.db.models import Count
from django.utils.translation import gettext_lazy as _
# from stigapp.models import Author, Reaction, Message, Score
from stigapp.models import (
    DiscordMessage,
    DiscordReaction,
    GardenMessage,
    Log,
    RewardReactionLog,
    Rule,
    TopMessage,
    ExtractedUrls,
    # Urls
)


# TODO: Get a custom class working for Integerfields to show counts
# class CustomFieldListFilter(admin.FieldListFilter):
#     # fields = (models.IntegerField)

#     def __init__(self, field, request, params, model, model_admin, field_path):
#         self.lookup_kwarg = field.name
#         self.lookup_kwarg_more = field.name + "__gte"
#         self.lookup_kwarg_less = field.name + "__lt"
#         self.lookup_val = request.GET.get(self.lookup_kwarg, None)
#         super(CustomFieldListFilter, self).__init__(field, request, params, model, model_admin, field_path)

#         print("INIT VALUES==================")
#         print("lookup_kwarg")
#         print(self.lookup_kwarg)
#         print("lookup_kwarg_more")
#         print(self.lookup_kwarg_more)
#         print("lookup_kwarg_less")
#         print(self.lookup_kwarg_less)
#         print("lookup_val")
#         print(self.lookup_val)
#         print("=============================")

#     def expected_parameters(self):
#         return [self.lookup_kwarg, self.lookup_val]

#     def choices(self, changelog):
#         options = (
#             (_('All'), None),
#             (_('Less than 5'), '0'),
#             (_('More than 5'), '5')
#         )

#         for k, v in options:
#             print("QUERYSTRING=====================")
#             print("GET QUERY STRING")
#             print(changelog.get_query_string({self.lookup_kwarg + '__lte': v}))
#             print("QUERYSET")
#             print(self.queryset)
#             print("self.used_parameters")
#             print(self.used_parameters)
#             print("================================")

#             yield {
#                 'selected': self.lookup_val == v,
#                 'query_string': changelog.get_query_string({self.lookup_kwarg: v}),
#                 'display': k
#             }

#     # def queryset(self, request, queryset):
#     #     if self.value() == 'Less than 5':
#     #         print("less-----------------")
#     #         return queryset.filter(
#     #             unique_user_count__lte=4
#     #         )
#     #     if self.value() == '5 or more':
#     #         print("more+++++++++++++++")
#     #         return queryset.filter(
#     #             unique_user_count__gte=5
#     #         )


class DiscordMessageAdmin(admin.ModelAdmin):
    model = DiscordMessage
    # list_display = [field.name for field in DiscordMessage._meta.get_fields()]

    list_display = [
        'id',
        'created_at',
        'author',
        'content',
        'reactions',
        'saved_at',
        'jump_url',
        'message_type',
        'author_discord_username',
        'authorship_datetime',
        'channel_name',
        'guild_name',
        # 'extracted_urls_id'
    ]

    list_filter = [
        'created_at',
        'edited_at',
        'message_type',
        'channel_name',
        'guild_name'
    ]

    search_fields = [
        'created_at',
        'edited_at',
        'content',
        'author',
        'message_type',
        'forum_tags'
    ]


class DiscordReactionAdmin(admin.ModelAdmin):
    model = DiscordReaction
    list_display = [field.name for field in DiscordReaction._meta.get_fields()]


class GardenMessageAdmin(admin.ModelAdmin):
    model = GardenMessage
    list_display = (
        "id",
        "editable_message",
        "edited_at",
        "forum_tags",
        "twitter_handle",
        "user_id",
        "original_message",
        "reactions",
        "unique_reaction_count",
        "unique_emoji_list",
        "counts_by_emoji",
        "unique_user_count",
        "unique_user_list",
        "created_at",
        "exported_at",
        # "extracted_urls_id"
    )
    list_editable = (
        "editable_message",
        "forum_tags",
        "twitter_handle"
    )

    list_filter = [
        'created_at',
        # ('edited_at', admin.BooleanFieldListFilter),
        'message_type',
        'unique_user_count',
        'unique_reaction_count',
        # ('exported_at', admin.BooleanFieldListFilter),
        # ('twitter_handle', admin.BooleanFieldListFilter)
    ]

    search_fields = [
        'author',
        'author_discord_username',
        'twitter_handle',
        'message_id',
        'original_message',
        'edited_at',
        'message_type',
        'unique_user_list',
        'emoji_list',
        'forum_tags'
    ]

    readonly_fields = ['user_id', 'original_message', 'reactions']

    def get_readonly_fields(self, request, obj=None):
        # Can distinguish between superusers and staff
        # if request.user.is_superuser:
        #     return []
        return self.readonly_fields


class LogAdmin(admin.ModelAdmin):
    model = Log
    list_display = [field.name for field in Log._meta.get_fields()]


class RewardReactionLogAdmin(admin.ModelAdmin):
    model = RewardReactionLog
    list_display = [field.name for field in RewardReactionLog._meta.get_fields()]


class RuleAdmin(admin.ModelAdmin):
    list_display = (
        "rule_name",
        "threshold",
        # "chosen_reactions",
        # "channel_name",
        # "role",
        # "activated_status"
    )
    list_editable = (
        "threshold",
        # "chosen_reactions",
        # "channel_name",
        # "role",
        # "activated_status"
    )


class TopMessageAdmin(admin.ModelAdmin):
    model = TopMessage

    # list_display = [field.name for field in TopMessage._meta.get_fields()]
    list_display = [
        'id',
        'added_to_garden',
        "editable_message",
        "edited_at",
        'author_discord_username',
        'original_message',
        'reaction_count',
        'unique_reaction_count',
        'emoji_list',
        'unique_emoji_list',
        'counts_by_emoji',
        'user_count',
        'unique_user_count',
        'user_list',
        'unique_user_list',
        "created_at",
        "exported_at",
        # "extracted_urls_id"
    ]
    list_editable = (
        "added_to_garden",
        "editable_message",
    )
    list_filter = [
        'created_at',
        'message_type',
        'unique_user_count',
        'unique_reaction_count',
        ('added_to_garden', admin.BooleanFieldListFilter),
    ]

    search_fields = [
        'author',
        'author_discord_username',
        'message_id',
        'original_message',
        'edited_at',
        'message_type',
        'unique_user_list',
        'emoji_list',
        'forum_tags'
    ]

    ordering = ['-reaction_count']


class ExtractedUrlsAdmin(admin.ModelAdmin):
    model = ExtractedUrls
    # list_display = [field.name for field in ExtractedUrls._meta.get_fields()]
    list_display = [
        'id',
        'url_list'
    ]


# class UrlsAdmin(admin.ModelAdmin):
#     model = Urls
#     list_display = [field.name for field in Urls._meta.get_fields()]


admin.site.register(DiscordMessage, DiscordMessageAdmin)
admin.site.register(DiscordReaction, DiscordReactionAdmin)
admin.site.register(ExtractedUrls, ExtractedUrlsAdmin)
# admin.site.register(Urls, UrlsAdmin)
admin.site.register(GardenMessage, GardenMessageAdmin)
admin.site.register(Log, LogAdmin)
admin.site.register(RewardReactionLog, RewardReactionLogAdmin)
admin.site.register(Rule, RuleAdmin)
admin.site.register(TopMessage, TopMessageAdmin)


# admin.site.register(Author)
# admin.site.register(Reaction)
# admin.site.register(Message)
# admin.site.register(Score)
