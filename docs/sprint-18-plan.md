# Sprint 18 (Dec 6 - Dec 13)

## Community profile 

* [x] R?: Fix the word "The" at end of line for airtable text box field
* [x] R?: Hide the twitter handle field (redundant)
* [x] R?: Put discord and twitter hand next to each other
* [x] R?: Remove the first column in the airtable credentials section on the **community profile** page
* [] R?: Add mastodon field and database column or jsonfield key
* [] R?: long textbox inputs should be below the prompt/instructions
* [x] R?: Split json fields into appropriate subfields
* [] R?: Write sentence around "Promotion threshold" and embed number widget within it. 
* [x] R?: Size widgets according to their value str length (e.g. Promotion threshold should be 3 digits wide at most)
* [x] R?: Use toggle switch rather than checkbox for boolean twitter handle request
* [x] R1: Make the community profile prettier and simple to use

## DOCUMENT the backend behavior of the bot

* [x] R?: Look at all the Community Profile settings and document what they cause to happen.

  * 1 airtable
  * 1 guild (server)
  * all channels or channel list (fine even if it's not yet using)

## User profile

* [x] R?: Change the username field to Discord tag 
* [] R?: **Cancel** and **Save** buttons of the user profile page should redirect to the **conmmunity profile** page

## Additional frontend

* [x] R?: Add two pages for **Privacy Policy** and **ToS**
* [x] R?: Add links to the **ToS** and **Privacy Policy** page on the **About** page
* [] R?: Redirect users after they signup to the **user profile** page
* [] R?: Redirect users after they sign in to the **community profile** page
* [] R?: Add buttons to accept **ToS** and **Privacy Policy**
* [] R?: Insert the **ToS** page into the flow after signin with Discord
* [] R?: Insert **Privacy Pol** into flow as checkbox and link within **ToS** Page

## Backend

* [] R?: Display on user profile their role [admin(database administrator), staff(community manager), user(guest)]
* [] R?: Make the users login first to see the profile and community pages
* [] R?: Sign up should redirect to the profile page
* [] R?: Sign in should redirect to the community page
* [] R?: Get the **community name** field working (`discordlogin/views.py`)
    1. save it to the database
    2. research how to retrieve the logged in user id
    3. retrieve the logged in user object
    4. retrieve the community name for the logged in user using CommunityProfile.objects.get(user=loggedin_user)
* [] R?: Get the reaction threshold field working 
    1. save it to the database
    2. read it from the database when the code needs it (log_messages_dis.. file)

* [] R?: Get the Twitter handle request filed working
* [] Get the ** Airtable table name** field working 
* [] Get the ** Airtable base id** field working 
* [] Get the ** Airtable API key** field working 
* [] R: enable guild ids list used to filter `log_discord_messages.py` during checking of the threshold rule (and all other rules)
* [] R1: fancy low priority feature: try to find out which discord servers that the bot has been invited to by this staff user on Stigsite associated with this Community Profile
* [] R1: Community profile threshold connected to log_discord_message.py (change `get_reaction_threshold_value()`  function to use Django query instead of Supabase query)
  x Query community profile for Discord user id
  x Query the Pycord API with user id to find Discord servers associated with that user id
  x Save that list of servers (guild) in the community profile
* [] R: stigsite-tec(dogfood).onrender.com -> stigflow.onrender.com domain name in render.com dashboard

## Lower priority

* [] R1: Email/password signup in addition to Discord
* [] R?: Sort and filter columns in **Messages** table

## Airtable (TBD if coordinated with TEC and TEA)

- "added to garden" field name
- "exported at" field name
