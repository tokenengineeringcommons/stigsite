# UX Design Notes

## Ronen + Maria User Stories

MARIA/RONEN: stories are features of software that can be implemented now, so if you want us to monitor discord channels we need that list of channels immediately
* [ ] TEC will provide the list of monitored channels and the Threshold rules to the team ASAP (at least 1 week before the pilot)

* [-] Once message on monitored channels satisfied the Threshold Rules, it is classified as a TopMessage within 5 minutes.
* [x] Once message is selected for gardening (by an admin user in the TopMessage) it is copied to the GardenMessage.
* [x] Gardener can see in Greenhouse (stigsite.onrender.com/admin) every message classified as GardenMessage (by admin users of stigsite).
* [-] stigbot+django-signal: Once a message is classified as Garden Message, the bot reacts to it in discord with a special Garden emoji
* [x] admin: Gardener can remove a message from the GardenMessage using the admin interface
* [x] admin: Gardener can see the text of the original Greenhouse message, who posted it, and how many reactions it got originally
* [x] admin: Gardener can edit the text of the GardenMessage, and see both original (in TopMessage Table) and edited version (edited version does NOT get synced to TokenGated Channel)
* [ ] admin: Gardener can edit the text of the GardenMessage, and see the original message next to it in the same GreenhouseMessage table

## OBE (Not currently planned for MVP or Pilot)

TODO MARIA/RONEN: clarify these features and define "Twitter view" and Token Gated Channel now that those have been eliminated from the plan (by Shahar).
Break stories down into the smallest possible user interface affordances

* When a Gardener marks a message in Token Gated Channel with Twitter emoji, it can been seen in Twitter view in the Greenhouse
* V1 end of Twitter flow: Gardener can exported to CSV the messages in Twitter view (that haven't been exported yet). *TEC team will provide the exact fields and new field names.*
* V2 end of Twitter flow: Gardener can push to Airtable the messages in Twitter view (that haven't been pushed to Airtable yet).
* When a Gardener marks a message in Token Gated Channel with research base emoji, it can be seen in Research Base view in the Greenhouse.

**Open Questions:**
1) Do we want to save other people's reactions from token gated channel? If so, where they will be available to view?
2) What happens to the Research Base messages after they are marked as such?
Collapse

## Shahar 2022-09-12
* DONT DO: [ ] Once a message is classified as Garden Message, it is pushed into the Token Gated Channel


## Potential workflow for TEC (by Shahar 2022-09-06)

Zoom recording will be posted on Discord Sensemaking-Dev channel.

1. someone posts a message on discord on one of the channels stigbot follows
2. people react to that message with emojies in Discord
3. after N reactions a message is added to a leaderboard (Top Messages)
4. after N reactions it is added to a token gated Discord channel at TEC (Hobson has been invited)
5. people in the TEC community react to the message on the token gated Discord channel
6. if anyone reacts with Twitter bird the message is forwarded to the Garden Discord channel and stigbot reacts to the original Discord message (see step 1&2) with a flower emoji or some other "planted" emojie and/or replies with the message "Planted!"
7. TEC twitter marketing manager exports CSV of the Garden messages from stigsite.onrender.com/garden/
8. TEC twitter marking community manager imports the Garden messages into an  Airtable system with automation facilitating discussion with other TEC execs/managers (Airtable columns: Topic, Status (live/not), Reviewed by, Designed by, Content)
9. At 2022-08-06 call Shahar suggested he would, ("by Thursday") compose some rules deciding what rows and columns are needed for the Garden table (used for CSV export or one-way sync to Airtable) 

Alternative to Airtable preferred by Tangible AI is to use another TEC channel for the gardeners to have their discussion/meetings and conversation.

## Four different "users"

Each user has different needs

1. admin-user: admins creating rules to populate leaderboad automatically
2. internal-user: internal person in the DAO community
3. gardner-user (worker bee): gardeners editing and curating messages into content
4. end-user (public): community consuming and feedbacking on messages (token-gated channel?) - twitter channel or special token-gated dicsord channel

### 1. admin-user: Maria's rule-config wireframe

Admins need to be able to create a list of emojies, user roles, and channels that that trigger the creation of a leaderboard entry for a message.

* Quantity/count threshold field
* Choose emoji (grid of 100's of emojies) [with add button]

- List of reactions that qualify for the count: ...

* Select Channel: Pulldown
* Select Role: [Gardener|Contributor|MEE6|Steward|Member|daotest|All Roles]

- List of roles that qualify for the count threshold: ...

### 2. internal-user: TEC (example DAO) employees 

Discord channel where internal token-gated TEC/DAO community members compose messages brainstorming about the business and voting of the DAO.

### 3. gardner-user: Designated content curators for TEC community

Reading and editing the leaderboard.
Selecting a subset for release to end-user (potentially public).

### 4. end-user: twitter users

Followers of the official TEC/DAO community.

## Data architecture

### Supabase

discord_messages
discord_reaction individual reaction instance (evntually fk to discord_message)

### Stigsite DB (sqlite or postrgress)

destroyed and recreated from scratch with each deploy

stigsite/stigapp/models.py
stigsite/dicordlogin/models.py
