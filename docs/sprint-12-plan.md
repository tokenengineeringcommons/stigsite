# Sprint-12

Wed Oct 26, 2022 

- [x] test for authorship and date fields and other new fields in the Airtable on staging (DAOStack's airtable invite)
- [x] Extracted URLs
  - [x] G8-3.5: extract URLs from discord messages and store them in new URL(Model) table with FK back to discord_message TopMessage and GardenMessage tables
  - [x] G3-2.5: add a field with the first url in a message as field in Top messages and Garden
- [x] thread titles/names added as a field to all tables
  * [x] DM
  * [x] TM
  * [x] GM
  * [x] TM Airtable
  * [x] GM Airtable
- [?] G2-0.6: BUG?: messages posted as regular messages are labeled as "Reply" type rather than "Forum" type (e.g. https://discord.com/channels/810180621930070088/1022561169506062457/1022561169506062457)
   * [?] Type - Possibly resolved by refactoring (recreating the entire message object and almost all fields each time)
   * [x] Channel name - In forums, the channel name is showing up as the title instead of the channel name (need to use Thread.parent instead of channel)
- [x] G8-4.5: Refactor on_raw_reaction_add/remove
  * [x] Split check_top_message eligibility function into two focused functions
  * [x] Reduce repetition
  * [x] Make it simplier to add/change/remove fields

- [x] G0.5-0.5: Update documentation with new fields

- [x] G1-0.5: Update TM/GM views with:
  * [x] guild_name
  * [x] channel_name
  * [x] extracted_urls
  * [x] twitter_handle
  * [x] authorship
  * [x] editable_message
  * [x] edited_at

## Twitter handle feature

From shahar and Gideon: "Stigbot asks [with a reply in Discord] for Twitter handle" whenever it doesn't have a handle for that user.

- [x] G0.5-0.5: add twitter handle field to discord_message, top messages and garden messages tables and airtable syncs
  * [x] Add to Supabase tables
  * [x] Add to log_discord_messages.py
  * [x] Add to models.py
  * [x] Add to TM/GM Airtables
- [x] G0.5-0.1: add a twitter_username CharField() to the User or Profile data model in models.py
- Add twitter_username to profile page form/admin view
= [ ] populate the User or Profile table with 
- [x] G8-3: whenever a message is posted to the topmessages table for an author that has not yet provided their twitter handle, ask them for it in discord with a reply.  When they reply to that reply parse their reply to extract their discord handle, and add it to all the tables (including airtable syncs).
  * [x] If a user is not in Stigsite, the bot creates an account in Stigsite for them.
  * [x] Send request for Twitter handle only once
  * [x] Only accepts the first reply in the DM channel for the twitter handle.  Subsequence messages don't update the Twitter handle.
  * [x] Separate messages into a bot_prompts object and reference this object to get messages
  * [x] The user's twitter_handle response is not saved to the DM table
- [x] make sure stigbot messages never end up in the topmessages table (it's OK if they appear in the discord messages table) -
  - [ ] implementation idea: configurable rule like the threshold rule? in the same place as the threshold (CommunityProfile)?
  - [ ] implementation idea: a function named author_not_blacklisted() returns True/False by checking the authorship of the message and confirms it's not from a blacklist of topmessage authors (this blacklist which can be populated with the discord username of stigbot)
  - [ ] implementation idea: ANDed this rule with the reaction threshold rule in the check_eligability function 


## Site framework feature

Sites framework may allow us to have multiple communities, each with their own discord server and stigsite url, with one deployed instance of the Django stigsite app.

- [x] G1-2.5: more Site framework experimentation and testing
- [ ] add a Site foreign key to the DiscordMessageBase table


From Shahar: "
As for moving to garden bit, he would like to wait and think about it.
"

# Low Priority

Shahar asking TEC whether they want any of these:

- [x] when a message appears on discord if you don't yet have a twitter handle for the author ask for it and store it in a table of discord user handles
- [ ] reply to all messages that you don't
- [x] 0 reaction threshold
- [ ] add rule-action pair to accomplish the following action: if messages are reacted with a flower emoji it's promoted to becoming a GardenMessage

