## Sprint-13

### Zapier-like rule configuration prototype

- [x] G1-1: sign up for a free Zapier account and create an automation that does something with Telegram, Slack, an API like nudger.qary.ai, or Airtable
  - [x] do some automated action on a discord server ot airtable data table

- [ ] Connect Zapier to Stigsite through a REST API, websocket, or any other means of connecting python to Zapier 
  - [ ] allow Zapier users to turn on a hard-coded rule to react with the flower emoji to a particular test discord message through stigbot 
  - [ ] allow Zapier users to enter the keyword or phrase for the discord messages that they want stigbot to react to 
  - [ ] allow Zapier users to select for the emoji to use to react to discord messages that contain the keyword or phrase configured above

- [ ] Attempt a Zapier-like data model and Rule configuration UX based on `n8n`
  - [ ] find the database schema for rules or data structure in code within the Zapier-like app ("n8n") source code (https://github.com/n8n-io/n8n) to understand Rules data structure
  - [ ] create Trigger & Action & BinaryOperation tables in stigsite.models
  - [ ] populate Trigger & Action tables with data to implement top message threshold rule (reactions >= 0)
  - [ ] populate Trigger & Action tables with data to implement garden message threshold rule (reactions >= 0)
- [x] RULE: twitter handle reply/request should only ask once and never again
  - [ ] RULE (LOWPRIORITY): additional rule to ask for twitter handle if a message achieves N reactions threshold

### separate field name for forum thread_title (post title) and channel_name

- [x] BUG: channel field is providing the forum or thread_title rather than the parent channel_name
  - [x] SHOULD-BE: if message is a forum message type, populate a new thread_title field with the post title (which appears in the current system as a , and a message
  - [x] for a message in a forum (called a post) the channel_name field should contain the thread_title like "# stigflow-development"
  - [x] message in a thread (not a forum message type) should report the parent channel_name as the channel_name "# stigflow-development"

### Site framework feature

Sites framework may allow us to have multiple communities, each with their own discord server and stigsite url, with one deployed instance of the Django stigsite app.

- [x] G2-2.5: Implement the sites framework in Stigsite
  * Add Sites framework settings
  * Add CommunityProfile model with ForeignKey relation to Site
  * Add Site ForeignField to User
  * Make Profile page with updatable field for Site (Team URL)

### Incrementally modularize log_discord_messages.py -> stigbot/main.py

## Nice to have

- [x] make sure stigbot messages never end up in the top messages table (it's OK if they appear in the discord messages table) -
  - [ ] implementation idea: configurable rule like the threshold rule? in the same place as the threshold (CommunityProfile)?
  - [ ] implementation idea: a function named author_not_blacklisted() returns True/False by checking the authorship of the message and confirms it's not from a blacklist of topmessage authors (this blacklist which can be populated with the discord username of stigbot)
  - [ ] implementation idea: ANDed this rule with the reaction threshold rule in the check_eligability function 

## Low priority field names for forum post title, channel_name, and thread_title

- [ ] BUG: channel field is providing the forum or thread_title rather than the parent channel_name
  - [ ] SHOULD-BE: if message is a forum message type, populate a new thread_title field with the post title (which appears in the current system as a , and a message
  - [ ] for a message in a forum (called a post) the channel_name field should contain the thread_title like "# stigflow-development"
  - [ ] message in a thread (not a forum message type) should report the parent channel_name as the channel_name "# stigflow-development"

## Low Priority

- change bot name from "daotest" to "stigbot-csm"
  - [ ] this may only be possible for the person who created the DISCORD_KEY and invited the bot to a server
  - [ ] make the bot name configurable by the stigsite user who is labeled a community manager
  - [ ] make the stigsite url configurable by the stigsite user who is labeled a community manager (even if we just manually configure onrender with the project name, we use the database to do the reconfigure)
- change field name from "Jump Link" to "Link to Message"
- change name of "TopMessage" ->

- they're planning to use loomly social media management
- https://n8n.io/integrations/github-trigger/
- https://www.notion.so/m4co/Dev-sync-6efc637d4d2d4d33bf224f0f4f246518 

## Backlog

### Create Signup Flow

- [ ] EULA page
- [ ] Privacy policy
- [ ] H: go through the deploy.md instructions section for creating a new discord bot
- [ ] H: APIKEY for new bot name, AIRTABLE_THREAD_TITLE

