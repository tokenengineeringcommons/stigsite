# sprint-03-plan.md

### 2022-08-25 Greg:

- Home page for stigsite.onrender.com/ with link to stigsite.onrender.com/admin/ endpoint and contact information for you as the admin (greg@tangibleai.com) and maybe a link to DAOStack's website

### TODO:

To record additional information that discord discards, we need to maintain seperate lists of dictionaries with the previous reacitons (emojies) and the user that created the emoji reaction.
Currently in log_discord_messages.record_reaction() we use the discord-provided reaction.message.reactions list of reaction counts to update our supabase message.previous_reactions list of dictionaries, but that overwrites the user information in that list of dicts. 
Use our own supabase table field message.previous_reactions to keep track of (users, emoji) pairs for each message and have (emoji, count) pairs in separate field called reaction_counts.
Maybe better field names would be reaction_user_list and reaction_count_list (so the outer pluralization in Django admin interface looks right)
Our supabase message.previous reactions listofdicts contains the user for each reaction instead of just count


## Sprint 03 - Tues Aug 23 - Aug 30

- H: docs and sprint plans on stigsite
- H1: Greg access to discord qary server
- H1: invite greg to daostack daogarden server
- H: get supabase message recording working again
- H: models.py in Django that duplicate supabase schemas
- H1: connect supabase settings.DATABASES = {} to stigsite/app
- H1: connect supabase to render managed postgresql db
- H: upgrade stigsite to nonfree render VM
- H: log reactions to supabase
- H: create config syntax for counting/analytics on messages & reactions
- H: count reactions as they occur
- H: filter messages that are logged to supabased based on minimum reactions threshold
- H: syntax for tagging messages based on thresholds for reactions, reaction types, etc
- H: analytics/plots/tables on messages logged, reactions counted
- HGR: Django OAuth login with discord account

- G: models.py schema for config file data that choses the tasks, args and kwargs
- G: configure admin.py interface for all tables
- G: django create superuser with password from .env (add to .env in BitWarden) during build.sh

- M: get bubble connected to supabase


## Backlog

- Django signup
- Django login
- Django password reset
- Django edit user profile / avatar
- H1: redis + uvicorn + pycord + ascync environment

- G1: create fastapi demo site from tutorial
- G1: use huggingface python webapp package to create huggingface spaces 

## Sprint 02 Aug 11 - Aug 18

- H1-1: hello discord bot on qary discord server
- H1-1: supabase schema for holding messages
- H1-1: push one discord message to supabase
- H1-.5: access to env file on bitwarden and discord credentials
- H1-.5: send links and access to all repos in daostack
- H4-8: (failed) deploy stigdata to stigsite.onrender.com

- G1-1: sign up for discord and play with it and subscribe/join servers that interest you
- G4-3: write down and organize notes about what all these files are and what they are for (in docs
- G4-4: go through the discord python bot examples that you can find (discord-py)
- G2-1.5: modify the example discord bot from tutorial to retrieve many messages from discord history
- G4-1: deploy empty django app stigsite to render stigsite.onrender.com or similar 







site for a text box input/output


## Sprint 01

- got a discord bot (stigdata) working with qary server `/hello` pycord+uvicorn+redis


