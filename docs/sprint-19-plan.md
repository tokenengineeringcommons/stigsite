# Sprint 19 (Dec 14 - Dec 21)

* [] R?: Add question mark icons with helpful popup tips on each field
* [x] R0.1-0.1: Use "Twitter handle" and "Discord handle" everywhere for "username" in docs of signup flow
* [x] R0.1-0.1: Use "Twitter handle" and "Discord handle" everywhere for "username" in UX (HTML)
* [] R?: Put community name in header on all pages/views 
* [] R?: Put community name in community profile title
* [] R?: Replace TEC's in footer with community name from database (CommunityProfile)
* [x] R0.1-0.1: Remove "© 2022 Token Engineering Commons"
* [x] R0.1-0.1: Disable/hide guild "tag" field (monitored guilds)
* [] R?: Add read-only field ({%template tag%} in template.html) guild name
* [x] R0.2-0.2: Add monitored channels field using the tag widget
- use log_discord_messages populate channel list for that communityprofile with the channels from the guild/server that the message came from
- add button or tag "*" for all channels
- Community Profile [Save] button redirects to Messages table
- Community Profile [Save] button redirects to popup with help/instructions/docs before revealing messages table underneath.
    - popup: "Go to your Discord server and react to one of your discord messages and watch it appear here!!!"


- badge or icon or coloring to indicate when user logged in is staff


### low priority
- User table need blob for avatar or path to media/ where avatars are stored
- During download of message in log_discord_message download image of all users and add to DB

### next year
- User profile should have database field and default image for avatar
- User profile way for them to upload manually an avatar image
- Download discord profile community/guild image/logo using `pycord` and add to UserProfile
- Download discord profile image using `pycord` and add to UserProfile

- On sign **UP** redirect to user profile
- On sign **IN** redirect to community profile

### notes
- n8n can copy and paste messages from one place to another (discord Airtable, it has user name but not image).