AUTHOR_ATTRS = 'id name discriminator bot display_name'.split()

REACTION_ATTRS = 'emoji count me user message.id'.split()
REACTION_COROS = 'users'.split()  # coroutines

ALL_REACTION_ATTRS = 'count emoji me message'

# https://docs.pycord.dev/en/stable/api.html?highlight=user#discord.User
USER_ATTRS = 'id name display_name discriminator bot system mention dm_channel'.split()
ALL_USER_ATTRS = ('accent_color accent_colour avatar banner bot color colour created_at default_avatar'
                  ' discriminator display_avatar display_name dm_channel id jump_url mention'
                  ' mutual_guilds name public_flags system').split()
