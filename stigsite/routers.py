""" stigsite/stigsite/routers.py

References

- [Bjoern-Holste](https://scribe.rip/https%3A/bjoern-holste.medium.com/django-hearts-supabase-8b251f77d206)

"""


# class CustomRouter(object):

#     def db_for_read(self, model, **hints):
#         # if model._meta.db_table == 'auth_user' or model._meta.db_table == 'django_admin_log':
#         #     return getattr(model, "_DATABASE", "supabase")
#         return getattr(model, "_DATABASE", "default")

#     def db_for_write(self, model, **hints):
#         # if model._meta.db_table == 'auth_user' or model._meta.db_table == 'django_admin_log':
#         #     return getattr(model, "_DATABASE", "supabase")
#         return getattr(model, "_DATABASE", "default")

#     def allow_relation(self, obj1, obj2, **hints):
#         """ Relations allowed if both tables in the master/slave pool. """
#         db_list = ('default', 'remotedata')
#         return obj1._state.db in db_list and obj2._state.db in db_list

#     def allow_migrate(self, db, app_label, model_name=None, **hints):
#         """ All non-auth models end up in this pool. """
#         return True


# class UserRouter(object):

#     def db_for_read(self, model, **hints):
#         print("TEST!!!!!!!!!!!!!!!!!!!!!!!!!!")
#         if model._meta.db_table == 'auth_user':
#             return 'supabase'
#         return getattr(model, "_DATABASE", "default")

#     def db_for_write(self, model, **hints):
#         print("TEST2**************************")
#         if model._meta.db_table == 'auth_user':
#             return 'supabase'
#         return getattr(model, "_DATABASE", "default")

#     def allow_relation(self, obj1, obj2, **hints):
#         print("TEST3^^^^^^^^^^^^^^^^^^^^^^^")
#         """ Relations allowed if both tables in the master/slave pool. """
#         db_list = ('default', 'remotedata')
#         return obj1._state.db in db_list and obj2._state.db in db_list

#     def allow_migrate(self, db, app_label, model_name=None, **hints):
#         """ All non-auth models end up in this pool. """
#         return True
