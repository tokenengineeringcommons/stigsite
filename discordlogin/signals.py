from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import User
from stigapp.models import GardenMessage


@receiver(post_save, sender=User)
def update_twitter_handles(sender, instance, created, **kwargs):
    """ Updates the twitter_handle values in GardenMessage posts if a User instance has a twitter_handle

    Will not overwrite existing twitter_handle in the GardenMessage table
    """
    if getattr(instance, 'from_admin_site', True):
        if instance.twitter_handle:
            GardenMessage.on_site.filter(
                author_discord_username=instance.discord_tag
            ).filter(
                twitter_handle__isnull=True
            ).update(
                twitter_handle=instance.twitter_handle
            )
