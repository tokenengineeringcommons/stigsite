from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
# from .models import DiscordUser


from .forms import UserChangeForm, UserCreationForm
from .models import User, CommunityProfile


class UserAdmin(BaseUserAdmin):
    """ Controls the admin view of the custom User model """
    form = UserChangeForm
    add_form = UserCreationForm
    actions = ['delete_selected']

    list_display = ('discord_tag', 'is_admin')
    # list_filter = ('admin',)

    list_filter = [
        'admin',
        ('twitter_handle', admin.BooleanFieldListFilter),
    ]

    search_fields = [
        'username',
        'discord_tag',
        'twitter_handle',
    ]

    fieldsets = (
        (None, {
            'fields': (
                'username',
                'discord_tag',
                'twitter_handle',
                'first_name',
                'last_name',
                'avatar',
                'public_flags',
                'flags',
                'locale',
                'active',
                'staff',
                'admin',
                'email',
                'password')
        }),
    )

    # Adds fields to the default user creation form
    add_fieldsets = (
        (None, {
            'fields': (
                'discord_tag',
                'twitter_handle',
                'twitter_username',
                'id',
                'password1',
                'password2'
            )
        }),
    )

    def delete_selected(self, request, obj):
        """ TODO: Find a way to avoid manually making databases to support this

        Currently relies on auth_user and auth_user_user_permissions even though there isn't any data in them

        """
        for user in obj.all():
            User.objects.get(id=user.id).delete()

    def save_model(self, request, obj, form, change):
        """ Saves a User instance and adds whether it is from the admin site or not """
        obj.from_admin_site = True
        super().save_model(request, obj, form, change)


# Register your models here.
# admin.site.register(DiscordUser)
admin.site.unregister(Group)
admin.site.register(User, UserAdmin)
admin.site.register(CommunityProfile)
# admin.site.register(CustomSite)
