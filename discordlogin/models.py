import datetime
import hashlib
import time

from django.contrib.auth.models import (
    BaseUserManager,
    UserManager,
    AbstractBaseUser,
    AbstractUser,
)
from django.contrib.sites.models import Site, _simple_domain_name_validator, SiteManager
from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _

# from django.utils.translation import gettext_lazy as _

# Community profile settings


class CommunityProfile(models.Model):
    id = models.BigAutoField(primary_key=True)
    airtable_credentials = models.JSONField(
        blank=True,
        null=True,
        help_text="The Airtable key, base name, and table name associated with Stigsite"
    )
    monitored_channels = models.JSONField(
        blank=True,
        null=True,
        help_text="A list of channels within guilds the site will listen to"
    )
    monitored_guilds = models.JSONField(
        blank=True,
        null=True,
        help_text="A list of Discord guilds (servers) the site will listen to"
    )
    promotion_threshold = models.IntegerField()
    site_style = models.JSONField(
        blank=True,
        null=True,
        help_text="The Airtable key, base name, and table name associated with Stigsite"
    )
    community_name = models.CharField(max_length=100, blank=True, null=True)
    django_site_id = models.ForeignKey(
        Site,
        db_column='django_site_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )
    community_members = models.JSONField(
        blank=True,
        null=True,
        help_text="A list of whitelisted members' Discord usernames"
    )
    enable_twitter_handle_request = models.BooleanField(default=False)

    class Meta:
        managed = False
        db_table = 'community_profile'

    def get_absolute_url(self):
        return reverse('community-profile', kwargs={'pk': self.pk})

    def check_unique_community_name(self, current_profile_id, current_profile_name_value):
        """
        Checks whether a profile is attempting to use the name of an existing profile

        Input:
        current_profile_id: id number of the profile being changed
        current_profile_name_value: the name the profile is changing to

        Output:
        Boolean
        """
        check = True
        community_profiles = CommunityProfile.objects.all()
        for profile in community_profiles:
            if profile.id == current_profile_id:
                continue
            if profile.community_name is None:
                if profile.community_name == current_profile_name_value:
                    check = False
                    return check
            elif profile.community_name.lower() == current_profile_name_value.lower():
                check = False
                return check
        return check

    def clean(self):
        """
        Calls appropriate checks on a CommunityProfile form submission.

        If all checks are passed, called the save method for Site and CommunityProfile

        If fails, raises a ValidationError
        """
        unique_community_name = self.check_unique_community_name(self.id, self.community_name)
        if unique_community_name:
            if self.community_name is None or self.community_name == '':
                raise ValidationError(_('The community_name cannot be empty'), code='invalid')
            site = Site.objects.get(pk=self.django_site_id.id)
            site.name = self.community_name
            site.save()
            self.save()
        else:
            raise ValidationError(_('Not a unique id'), code='invalid')
            print("Not unique")
            pass


class UserManager(UserManager):

    def check_for_admin_authorization(self, discord_tag):
        """ Checks against a hardcoded list to see if a user is approved for admin privileges"""

        check = [False, False]
        approved_staff_users = [
            'Sha#6179',
            'richa#0775',
            'GideonRo#3175',
            'Irem#3362',
            'enti#1546',
            'bear100#9085',
            'acidlazzer#5796',
        ]

        approved_admin_users = [
            # 'insert discord_tags',
            'thompsgj#8407',
            'hobs#3386',
            'hobs#9238',
            'ronent#2267',
            'mariadyshel#6267',
            'rochdikhalid#7903',
        ]
        if discord_tag in approved_staff_users:
            check = [True, False]
        elif discord_tag in approved_admin_users:
            check = [True, True]

        return check

    def get_hash_value(self, val=time.time()):
        hash = hashlib.sha1()
        hash.update(str(val).encode('utf-8'))

        return hash.hexdigest()[:7]

    def create_password(self):
        """  Makes a string for the mandatory, non-nullable password field.

        The password will not work even if a user knows it because authentication happens through Discord.
        """
        password = self.get_hash_value()
        return password

    def create_new_discord_user(self, user, password=None):
        """ Creates a user in the DiscordUser table.

        Uses the 'DiscordAuthenticationBackend' for authentication, not the standard Django auth backend.
        """

        # Sets up a default site for the user

        hash_value = self.get_hash_value()
        rehashed_value = self.get_hash_value(hash_value)
        site_name = f"temp{rehashed_value[0:4]}"
        site_domain = f"{site_name}.com"

        site_result = Site.objects.create(
            name=site_name,
            domain=site_domain
        )

        # Sets up a default community profile for the user
        community_profile = CommunityProfile.objects.create(
            airtable_credentials='{"key", ""}',
            monitored_channels='{"included", ""}',
            monitored_guilds='{"included", ""}',
            promotion_threshold=5,
            django_site_id=site_result,
            community_members='{"included": ""}',
            enable_twitter_handle_request=False
        )

        # Add a site
        # Add a community profile with a site (hash)
        # Add site to community profile

        discord_tag = '%s#%s' % (user['username'], user['discriminator'])
        permissions_arr = self.check_for_admin_authorization(discord_tag)

        new_user = self.model(
            email='test@test.com'
        )
        new_user.id = user['id']
        new_user.username = discord_tag
        new_user.discord_tag = discord_tag
        new_user.twitter_handle = ''
        new_user.twitter_username = ''
        new_user.avatar = user['avatar']
        new_user.public_flags = user['public_flags']
        new_user.flags = user['flags']
        new_user.locale = user['locale']
        new_user.mfa_enabled = user['mfa_enabled']
        new_user.active = True
        new_user.staff = permissions_arr[0]
        new_user.admin = permissions_arr[1]
        new_user.django_site_id = site_result
        new_user.community_profile_id = community_profile

        password = self.create_password()
        new_user.set_password(password)
        new_user.save(using=self._db)
        return new_user


class User(AbstractUser):
    """ A user who has logged in to stigsite through Discord """

    objects = UserManager()

    # Discord-specific user attributes
    id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=100, unique=True)
    discord_tag = models.CharField(max_length=100, unique=True)
    twitter_handle = models.CharField(max_length=100, blank=True, null=True)
    twitter_username = models.CharField(max_length=100, blank=True, null=True)
    first_name = models.CharField(max_length=100, null=True)
    last_name = models.CharField(max_length=100, null=True)
    avatar = models.CharField(blank=True, null=True, max_length=100)
    public_flags = models.IntegerField()
    flags = models.IntegerField()
    locale = models.CharField(max_length=100)
    mfa_enabled = models.BooleanField()
    date_joined = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(default=timezone.now)

    django_site_id = models.ForeignKey(
        Site,
        verbose_name="Team URL",
        db_column='django_site_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )
    community_profile_id = models.ForeignKey(
        CommunityProfile,
        db_column='community_profile_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )
    # django_customsite_id = models.ForeignKey(
    #     Site,
    #     verbose_name="Team URL",
    #     db_column='django_customsite_id',
    #     related_name="+",
    #     on_delete=models.CASCADE,
    #     null=True
    # )
    # Django admin user attributes
    # authenticated = models.BooleanField(default=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=True)
    admin = models.BooleanField(default=True)
    email = models.CharField(max_length=255, default="test@test.com")
    requested_twitter_handle = models.BooleanField(default=False)

    USERNAME_FIELD = 'discord_tag'
    REQUIRED_FIELDS = []

    class Meta:
        managed = False
        db_table = 'auth_user'
        # app_label = 'user_data'

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.discord_tag

    def get_short_name(self):
        return self.discord_tag

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_active(self):
        return self.active

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_superuser(self):
        return self.admin

    @property
    def is_authenticated(self):
        return True

    def get_absolute_url(self):
        return reverse('profile', kwargs={'pk': self.pk})
