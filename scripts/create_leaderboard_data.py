from stigapp.models import *


class manage_leaderboard_data():
    """ Populates the TopMessage table in Supabase with the 10 messages with the highest reaction count

    Data is pulled from the DiscordMessage table

    >>> from scripts.create_leaderboard_data import *
    >>> up = manage_leaderboard_data()
    >>> up.generate_leaderboard_data()
    """

    def create_list_data(self, reaction_data, data_type):
        """ Returns an array of users or emoji from the reaction field

        Supports update_emojis_and_users_data
        """
        content_list = []
        for reaction in reaction_data:
            try:
                content_list.append(reaction[data_type])
            except KeyError:
                content_list.append(None)
        return content_list

    def generate_leaderboard_data(self):
        """ Clear the existing leaderboard and inserts the 10 messages with the highest reaction count
        """
        # TopMessage.objects.all().delete()

        # Get all messages in DiscordMessage table
        discord_content = [
            dict(
                author=m.author,
                message_id=m.id,
                created_at=m.created_at,
                text=m.content,
                reactions=[] if m.reactions is None else m.reactions,
                reaction_count=0 if m.reactions is None else len(m.reactions),
                jump_url=m.jump_url,
                forum_tags=m.forum_tags,
                message_type=m.message_type
            ) for m in DiscordMessage.on_site.all()]

        # Sort them in reverse order by reaction_count
        discord_content_sorted = sorted(
            discord_content,
            key=lambda content: content['reaction_count'],
            reverse=True
        )

        # Insert each into the TopMessage table (Supabase)
        for content in discord_content_sorted:
            user_list = self.create_list_data(content['reactions'], 'user')
            try:
                unique_user_list = list({v['id']: v for v in user_list}.values())
            except:
                unique_user_list = user_list
            unique_user_count = len(unique_user_list)

            # Get a list of emojis for a message
            emoji_list = self.create_list_data(content['reactions'], 'emoji')
            try:
                unique_emoji_list = list(set(emoji_list))
            except:
                unique_emoji_list = emoji_list
            unique_emoji_count = len(unique_emoji_list)

            # Get total count by reaction (emoji)
            counts_by_emoji_dict = {}
            for reaction in content['reactions']:
                counts_by_emoji_dict[reaction['emoji']] = len(
                    [i for i in content['reactions'] if i['emoji'] == reaction['emoji']])

            # Useful alternative condition for testing
            # if content['reaction_count'] > 4:
            if unique_user_count > 5:
                test = TopMessage.on_site.filter(
                    message_id=content['message_id']
                )
                if len(test) > 0:
                    pass
                else:
                    TopMessage.on_site.get_or_create(
                        author=content['author'],
                        author_discord_username=f"{content['author']['name']}#{content['author']['discriminator']}",
                        message_id=content['message_id'],
                        created_at=content['created_at'],
                        original_message=content['text'],
                        reactions=content['reactions'],
                        reaction_count=content['reaction_count'],

                        emoji_list=emoji_list,
                        user_count=len(user_list),
                        user_list=user_list,
                        unique_user_count=unique_user_count,
                        unique_user_list=unique_user_list,
                        unique_reaction_count=unique_emoji_count,
                        unique_emoji_list=unique_emoji_list,
                        counts_by_emoji=counts_by_emoji_dict,
                        jump_url=content['jump_url'],
                        forum_tags=content['forum_tags'],
                        message_type=content['message_type']
                    )


# ==================================================================

    # def generate_leaderboard_data_original(self):
    #     """ Inserts the 10 messages with the highest reaction count
    #     """
    #     # Get all messages in DiscordMessage table
    #     discord_content = [
    #         dict(
    #             author=m.author,
    #             message_id=m.id,
    #             text=m.content,
    #             reactions=[] if m.reactions is None else m.reactions,
    #             reaction_count=0 if m.reactions is None else len(m.reactions)
    #         ) for m in DiscordMessage.objects.all()]

    #     # Sort them in reverse order by reaction_count
    #     discord_content_sorted = sorted(
    #         discord_content,
    #         key=lambda content: content['reaction_count'],
    #         reverse=True
    #     )

    #     # Insert each into the TopMessage table (Supabase)
    #     for content in discord_content_sorted:
    #         if content['reaction_count'] > 4:
    #             test = TopMessage.objects.filter(
    #                 message_id=content['message_id']
    #             )
    #             if len(test) > 0:
    #                 pass
    #             else:
    #                 TopMessage.objects.get_or_create(
    #                     author=content['author'],
    #                     author_discord_username=f"{content['author']['name']}#{content['author']['discriminator']}",
    #                     message_id=content['message_id'],
    #                     original_message=content['text'],
    #                     reactions=content['reactions'],
    #                     reaction_count=content['reaction_count'])

# ============================================================

    # def create_list_data(self, reaction_data, data_type):
    #     """ Returns an array of users or emoji from the reaction field

    #     Supports update_emojis_and_users_data
    #     """
    #     content_list = []
    #     for reaction in reaction_data:
    #         try:
    #             content_list.append(reaction[data_type])
    #         except KeyError:
    #             content_list.append(None)
    #     return content_list

    # def update_emoji_and_user_columns(self):
    #     """ Extracts data from the reactions field to populate emoji_list, user_list, and user_count fields """
    #     messages = TopMessage.objects.all()

    #     for message in messages:
    #         # Get a list of users for a message
    #         user_list = self.create_list_data(message.reactions, 'user')
    #         unique_user_list = list({v['id']: v for v in user_list}.values())
    #         unique_user_count = len(unique_user_list)

    #         # Get a list of emojis for a message
    #         emoji_list = self.create_list_data(message.reactions, 'emoji')
    #         unique_emoji_list = list(set(emoji_list))
    #         unique_emoji_count = len(unique_emoji_list)

    #         # Get total count by reaction (emoji)
    #         counts_by_emoji_dict = {}
    #         for reaction in message.reactions:
    #             counts_by_emoji_dict[reaction['emoji']] = len(
    #                 [i for i in message.reactions if i['emoji'] == reaction['emoji']])

    #         TopMessage.objects.filter(
    #             original_message=message.original_message
    #         ).update(
    #             emoji_list=emoji_list,
    #             user_count=len(user_list),
    #             user_list=user_list,
    #             unique_user_count=unique_user_count,
    #             unique_user_list=unique_user_list,
    #             unique_reaction_count=unique_emoji_count,
    #             unique_emoji_list=unique_emoji_list,
    #             counts_by_emoji=counts_by_emoji_dict
    #         )
