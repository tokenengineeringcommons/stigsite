"""
This script migrates data between two databases.  The databases must be set up in settings.py, and the associated .env variables for access must be present in the .env file.

Inputs
- table_name: (str) The exact name of the table to be transferred
- origin_database: (str) The name of database the data will be transferred from
- receiving_database: (str) The name of the database the data will be transferred to

Trigger
- You must hardcode a call to the function `migrate_data` with the input values above for each
- Example: `migrate_data('ExtractedUrls', 'tecsupabase', 'default')`
"""

from dotenv import load_dotenv
load_dotenv()

from stigapp.models import DiscordMessage, TopMessage, GardenMessage, ExtractedUrls
from django.contrib.sites.models import Site


def get_existing_data_to_transfer(origin_database, table_name):
    """ Retrieves the existing data to be transferred from the origin_database and the table_headings for each field for the specified table

    Outputs:
    - entries_to_transfer: A QuerySet of all entries in the origin_database
    - table_fields: A list of field names (str) for each column

    """
    if table_name == 'ExtractedUrls':
        entries_to_transfer = ExtractedUrls.objects.using(origin_database).all()
        table_fields = [f.name for f in ExtractedUrls._meta.get_fields() if f.concrete]
    elif table_name == 'DiscordMessage':
        entries_to_transfer = DiscordMessage.objects.using(origin_database).all()
        table_fields = [f.name for f in DiscordMessage._meta.get_fields()]
    elif table_name == 'TopMessage':
        entries_to_transfer = TopMessage.objects.using(origin_database).all()
        table_fields = [f.name for f in TopMessage._meta.get_fields()]
    elif table_name == 'GardenMessage':
        entries_to_transfer = GardenMessage.objects.using(origin_database).all()
        table_fields = [f.name for f in GardenMessage._meta.get_fields()]

    return entries_to_transfer, table_fields


def upload_data(receiving_database, table_name, entry, upload_obj):
    """ Uploads the compiled data to the receiving_database

    Inputs
    - receiving_database: (str) The name of the database the data will be transferred to
    - table_name: (str) The exact name of the table to be transferred
    - entry: (obj) A row of data from the origin_database
    - upload_obj: (dict) The reformatted/serialized row data prepared for upload to the receiving_database

    Outputs
    - result: A database object if it was created
    - created: (bool) True or False depending on if a new object was created in the receiving_database
    """
    if table_name == 'ExtractedUrls':
        result, created = ExtractedUrls.objects.using(
            receiving_database
        ).update_or_create(
            id=entry.id,
            defaults=upload_obj
        )
    elif table_name == 'DiscordMessage':
        result, created = DiscordMessage.objects.using(
            receiving_database
        ).update_or_create(
            id=entry.id,
            defaults=upload_obj
        )
    elif table_name == 'TopMessage':
        del upload_obj['id']
        result, created = TopMessage.objects.using(
            receiving_database
        ).update_or_create(
            message_id=entry.message_id,
            defaults=upload_obj
        )
    elif table_name == 'GardenMessage':
        del upload_obj['id']
        result, created = GardenMessage.objects.using(
            receiving_database
        ).update_or_create(
            message_id=entry.message_id,
            defaults=upload_obj
        )
    return result, created


def migrate_data(table_name, origin_database, receiving_database):
    """ The core function that sends and receives data between the other functions

    NOTE: This assumes the receiving_database has the corresponding FK instance entries in the database already

    Output
    - A print statement on whether an entry was modified or created

    """
    entries_to_transfer, table_fields = get_existing_data_to_transfer(origin_database, table_name)

    # print("===============")
    # print("entries_to_transfer")
    # print(entries_to_transfer.count())
    # print("table_fields")
    # print(table_fields)
    # print("===============")

    upload_obj = {}

    for entry in entries_to_transfer:
        for field in table_fields:
            # Need to handle FK values separately because need model instance, not just id
            if field == 'django_site_id' or field == 'extracted_urls_id':
                val = getattr(entry, field, None)

                # If Null, use Null.  If it isn't Null, get an instance by filtering the receiving_database by the origin_database id val
                if val is not None:
                    if field == 'django_site_id':
                        fk_obj = Site.objects.using(
                            receiving_database
                        ).filter(id=val.id)
                    elif field == 'extracted_urls_id':
                        fk_obj = ExtractedUrls.objects.using(
                            receiving_database
                        ).filter(id=val.id)

                    upload_obj[field] = fk_obj[0]
                else:
                    upload_obj[field] = getattr(entry, field, None)
            else:
                upload_obj[field] = getattr(entry, field, None)

        result, created = upload_data(receiving_database, table_name, entry, upload_obj)
        if created:
            print(f"Created {result} in the database")
        else:
            print(f"Modified {result} in the database")

# ExtractedUrls is first to make sure the FK obj is there
# Sites were made manually, but could work by adding another migrate_data call
# migrate_data('ExtractedUrls', 'tecsupabase', 'default')
# migrate_data('DiscordMessage', 'tecsupabase', 'default')
# migrate_data('TopMessage', 'tecsupabase', 'default')
# migrate_data('GardenMessage', 'tecsupabase', 'default')
